<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\fileSystem\reader
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\fileSystem\reader\FileReaderException;
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	use nuclio\plugin\fileSystem\driver\common\CommonInterface;
	
	/**
	 * File information.
	 *
	 * This class provides main information of the file 
	 * 
	 */
	<<factory>>
	class FileReader extends Plugin
	{
		private CommonInterface $driver;
		private string 			$file;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):FileReader
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(string $driver, string $path)
		{
			parent::__construct();
			
			$tmpDriver=ProviderManager::request(...func_get_args());
			if ($tmpDriver instanceof CommonInterface)
			{
				$this->driver = $tmpDriver;
			}
			else
			{
				throw new FileReaderException(sprintf('"%s" is not a valid driver.',$tmpDriver));
			}
			if(!$this->driver->isFile($path))
			{
				throw new FileReaderException(sprintf('"%s" is not a valid file.',$path));
			}
			$this->file = $path;
		}

		/**
		 * Get the used driver name
		 * 
		 * @access private
		 *
		 * @return     CommonInterface  Driver Name.
		 */
		private function getDriver():CommonInterface
		{
			return $this->driver;
		}

		/**
		 * Get the full path.
		 * 
		 * @access public
		 *
		 * @return     string  Full path.
		 */
		public function getFullPath():string
		{
			return $this->file;
		}

		/**
		 * Check if the path given is a file.
		 * 
		 * @access public
		 * 
		 * @param  string  $path Path to the file.
		 * @return boolean       True/False for file or not.
		 */
		public function isFile():bool
		{
			return $this->getDriver()->isFile($this->file);
		}

		/**
		 * Check whether the param given is path or not.
		 * 
		 * @access public
		 * 
		 * @param  string  $directory Path to a directory
		 * @return boolean            True/False for the path or not.
		 */
		public function isDirectory():bool
		{
			return $this->getDriver()->isDirectory($this->file);
		}

		/**
		 * Check if the given path is readable.
		 * 
		 * @access public
		 * 
		 * @param  string  $path Path of directory
		 * @return boolean       True/False for writable or not.
		 */
		public function isReadable():bool
		{
			return $this->getDriver()->isReadable($this->file);
		}

		/**
		 * Check if the given path is writeable.
		 * 
		 * @access public
		 * 
		 * @param  string  $path Path of directory
		 * @return boolean       True/False for writable or not.
		 */
		public function isWritable():bool
		{
			return $this->getDriver()->isWritable($this->file);
		}

		/**
		 * Get content of a file.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return mixed       	Return file content if exist and error if file does not exist.
		 */
		public function read():string
		{
			if(!$this->isReadable())
			{
				throw new FileReaderException("Not readable!");
			}	
			return $this->getDriver()->read($this->file);
		}

		/**
		 * Get the file name.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File name
		 */
		public function getName():?string
		{
			return $this->getDriver()->getName($this->file);
		}

		/**
		 * Get the file extension.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File extension
		 */
		public function getExt():?string
		{
			return $this->getDriver()->getExt($this->file);
		}

		/**
		 * Get the file type.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File type
		 */
		public function getType():?string
		{
			return $this->getDriver()->getType($this->file);
		}

		/**
		 * Get the file size.
		 * 
		 * @access public
		 * @param  string $path Path to the file.
		 * @return int      	File size
		 */
		public function getSize():?int
		{
			return $this->getDriver()->getSize($this->file);
		}
	}
}
